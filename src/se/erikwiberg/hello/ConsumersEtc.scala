package se.erikwiberg.hello

object ConsumersEtc {
  def main(args: Array[String]): Unit = {
    
    val intProducer = () => List.range(1, 5)
    val print = (obj: Any) => println(obj)
    
    val double = (i: Int) => i * 2
    
    println("get intProducer data, map over it, and print it")
    
    transformAndConsumeDataSource(
        intProducer, 
        double,
        print)
    
    // optional type signatures
    val namesProducer: Function0[List[String]] = () =>  "kalle,omar,stina,erik,joppe".split(",").toList
    val nameGreeting: Function1[String,String] = (name: String) => String.format("Hello, %s! Welcome to Scala!", name)
    val toUpper: Function1[String,String] = (s: String) => s.toUpperCase()
    
   
    
    println("\nget namesProducer data, map over it, and print it")
    
    transformAndConsumeDataSource(
        namesProducer, 
        nameGreeting.andThen(toUpper), 
        print)
        
        
    println("\nIts the final countdown!")
    val countDownProducer = () => List.range(0, 10 + 1).reverse
    
    transformAndConsumeDataSource(
        countDownProducer,
        appendString(".."),
        print)
    
  }
  
  /**
   * Takes a functions that provides a list, transforms then consumes each item
   */
  def transformAndConsumeDataSource[T,R](
      dataSource: Function0[List[T]],
      transformer: Function1[T,R],
      consumer: Function1[R,Unit]) {
    
    dataSource.apply()
      .map(transformer)
      .foreach(consumer)
  }
  
  /**
   * returns a function that takes anything and appends the provided String
   */
  def appendString(stringToAppend: String): Function1[Any,String] = {
    (obj) => obj.toString() + stringToAppend
  }
}